# ur_application

## Initial setup
1. Clone `https://github.com/ros-industrial/universal_robot.git` in `catkin_ws/src`
2. Clone this repo (or your fork) in `catkin_ws/src`
3. From `catkin_ws`, run `rosdep install --from-paths src --ignore-src -r -y` to install system dependencies
4. Add a `conanfile.txt` at the workspace root with the following content:
```
[requires]
robot-model/1.0@bnavarro/stable
robot-trajectory/1.0@bnavarro/stable
robot-control/1.0@bnavarro/stable

[generators]
cmake
```
5. Add the `bnavarro` Conan remote: `conan remote add bnavarro https://navarrob.jfrog.io/artifactory/api/conan/navarrob-conan`
6. Configure Conan to work with this remote: `conan config set general.revisions_enabled=True`
7. Create a new Conan profile using the [C++11 ABI](https://gcc.gnu.org/onlinedocs/libstdc++/manual/using_dual_abi.html): `CONAN_V2_MODE=1 conan  profile new --detect default`. If you already have a default Conan profile, use this to make sure it is configured for the C++11 ABI: `conan profile update settings.compiler.libcxx=libstdc++11 default`
8. Get the Conan dependencies: `cd catkin_ws/build && conan install .. --build=missing`

## Build & install
Just run `catkin_make install` from `catkin_ws`

## Run
1. Make sure to source `catkin_ws/install/setup.bash` or `catkin_ws/install/setup.zsh` depending on your shell.
2. Launch the `dual_arm.launch` file: `roslaunch ur_application dual_arm.launch`

This launch file will load two UR10 arms inside the Gazebo simulator with joint position controllers.
